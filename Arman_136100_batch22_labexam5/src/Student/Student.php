<?php

namespace App\Student;
use App\Utility\Utility;
use App\Message\Message;
class Student
{
    public $id="";
    public $firstname="";
    public $middlename="";
    public $lastname="";
    public $conn;

    public function prepare($data){
        if(array_key_exists("id",$data))
        {
            $this->id=$data['id'];
        }
        if(array_key_exists("firstname",$data))
        {
            $this->firstname=$data['firstname'];
        }
        if(array_key_exists("middlename",$data))
        {
            $this->middlename=$data['middlename'];
        }
        if(array_key_exists("lastname",$data))
        {
            $this->lastname=$data['lastname'];
        }


    }
    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","labxm5b22") or die("connection failed");
    }
    public function store(){
        $query="INSERT INTO `labxm5b22`.`student` (`firstname`, `middlename`, `lastname`) VALUES ('" . $this->firstname."','" . $this->middlename ."','" . $this->lastname ."')";
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }
    public function index(){
        $_all=array();
        $query="SELECT * FROM `labxm5b22`.`student`";
        $result=mysqli_query($this->conn,$query);
        while ($row = mysqli_fetch_object($result)) {
            $_all[] = $row;
        }

        return $_all;
    }

    public function view()
    {
        $query = "SELECT * FROM `student` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `labxm5b22`.`student` SET `firstname` = '" . $this->firstname . "',`middlename` = '" . $this->middlename . "',`lastname` = '" . $this->lastname . "' WHERE `student`.`id` = " . $this->id;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
        <div class=\"alert alert-info\">
        <strong>Success!</strong> Data has been updated  successfully.
        </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `labxm5b22`.`student` WHERE `student`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
        <div class=\"alert alert-info\">
        <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
    <div class=\"alert alert-info\">
    <strong>Deleted!</strong> Data has not been deleted successfully.
        </div>");
            Utility::redirect("index.php");
        }

    }
}
