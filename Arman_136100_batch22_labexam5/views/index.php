<?php
session_start();
include_once('../vendor/autoload.php');
use App\Student\Student;
use App\Utility\Utility;
use App\Message\Message;

$object= new Student();
$all=$object->index();



?>


<html>
<head>
<title>Index</title>
</head>
<body>


    <h1><center>All Student List</center></h1>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <table class="table">
            <thead>
            <tr>
                <th>Student Serial</th>
                <th>ID</th>
                <th>first name</th>
                <th>last name</th>
                <th>Middle Name</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($all as $student){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $student-> id?></td>
                <td><?php echo $student->firstname?></td>
                <td><?php echo $student->lastname?></td>
                <td><?php echo $student->middlename?></td>

                <td>
                    <a href="create.php"  role="button">Create</a>
                    <a href="view.php?id=<?php echo $student-> id ?>"  role="button">View</a>
                    <a href="edit.php?id=<?php echo $student-> id ?>"  role="button">Update</a>
                    <a href="delete.php?id=<?php echo $student->id?>"  role="button" id="delete" Onclick="return ConfirmDelete()">Delete</a>

                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

</body>
</html>
