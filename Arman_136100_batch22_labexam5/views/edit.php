<?php

include_once ('../vendor/autoload.php');
use App\Student\Student;
use App\Utility\Utility;

$object= new Student();
$object->prepare($_GET);
$singleItem=$object->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-BOOK</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Student Info</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit First Name:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
            <input type="text" name="firstname" class="form-control" id="title" placeholder="Enter Book Title" value="<?php echo $singleItem->firstname?>">
            <label>Edit Middle Name:</label>

            <input type="text" name="middlename" class="form-control" id="title" placeholder="Enter Book Title" value="<?php echo $singleItem->middlename?>">
            <label>Edit Last Name:</label>

            <input type="text" name="lastname" class="form-control" id="title" placeholder="Enter Book Title" value="<?php echo $singleItem->lastname?>">
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
