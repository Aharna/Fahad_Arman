<?php
include_once ('../vendor/autoload.php');
use App\Student\Student;

$Object= new Student();
$Object->prepare($_GET);
$single=$Object->view();
?>
<html >
<head>
    <title>view info</title>

</head>
<body>
<a href="create.php"  role="button">Create New</a>
<br/>
<br/>
<a href="index.php"  role="button">Index page</a>
<br/>
<h1><center>Student Info</center></h1>
<div >

    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $single->id?></li>
        <li class="list-group-item">First Name: <?php echo $single->firstname?></li>
        <li class="list-group-item">Middle Name: <?php echo $single->middlename?></li>
        <li class="list-group-item">Last Name: <?php echo $single->lastname?></li>

    </ul>
</div>

</body>
</html>
