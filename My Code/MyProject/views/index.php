<?php
session_start();

include_once('../vendor/autoload.php');

use App\Book\Book;

$object= new Book();
$all=$object->index();
//var_dump($all);
?>
<html>
<head>
    <title>
        Index
    </title>
</head>
<body>
<h1>This is index page</h1>

<a href="create.php" role="button">Create</a>
<table  class="table-responsive">
    <thead>
        <tr>
            <th>#</th>
            <th>ID</th>
            <th>Title</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php
            $s1=0;
            foreach($all as $book){
                $s1++;
            ?>
            <td><?php echo $s1?></td>
            <td><?php echo $book-> id?></td>
            <td><?php echo $book-> title?></td>
            <td><a href="view.php?id=<?php echo $book-> id?>"  role="button">View</a>
                <a href="edit.php?id=<?php echo $book-> id?>" role="button">Edit</a>
                <a href="delete.php?id=<?php echo $book-> id?>" role="button" Onclick="return ConfirmDelete()">Delete</a>


            </td>
        </tr>
        <?php }?>
    </tbody>
</table>

<script>

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>
</body>


</html>
