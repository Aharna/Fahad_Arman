<?php

namespace App\Bitm\SEIP1020\Hobby;

use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

class Hobby{


    public $id= 'none';
    public $hobby= 'nothing';
    public $conn;


    public function prepare($data = "")
    {
        if (array_key_exists("hobby", $data)) {
            $this->hobby = $data['hobby'];
        }

        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

    }


    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomic_project");
    }

    public function store()
    {
        $query = "INSERT INTO `atomic_project`.`hobbies` (`hobby`) VALUES ('" . $this->hobby . "')";
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index()
    {
        $_allHobbies = array();
        $query = "SELECT * FROM `hobbies`";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allHobbies[] = $row;
        }

        return $_allHobbies;
    }

    public function view()
    {
        $query = "SELECT * FROM `hobbies` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function edit(){
        $array =array();
        $query = "SELECT * FROM `hobbies` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn,$query);
        if($row = mysqli_fetch_assoc($result)){
            $array[]= explode(",", $row['hobby']);
            //echo $row['hobby'];
           // var_dump($array[0]);
            return $array[0];
        }
        else
            echo "ERROR!";
    }

    public function update()
    {
        echo $this->hobby.$this->id;
        $query = "UPDATE `atomic_project`.`hobbies` SET `hobby` = '".$this->hobby."' WHERE `hobbies`.`id` = ".$this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been updated successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `atomic_project`.`hobbies` WHERE `hobbies`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }

}